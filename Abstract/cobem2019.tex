% LaTeX .tex
% Example for the proceedings of the  25th International Congress of Mechanical Engineering
% COBEM 2019
% October, 20-25, 2019, Uberlândia, MG, Brazil
% Based on the template of the proceedings of COBEM2015 and COBEM2017

\documentclass[10pt,fleqn,a4paper,twoside]{article}
\usepackage{abcm}
\def\shortauthor{R.W. Ferreira, A.F. Medina, M.B.X. Oliveira and D. Vieira}
\def\shorttitle{Rolling Moment Investigation of Wraparound Fins}

\begin{document}
\fphead
\hspace*{-2.5mm}\begin{tabular}{||p{\textwidth}}
\begin{center}
\vspace{-4mm}
\title{Rolling Moment Investigation of Missile Wraparound Fins}
\end{center}
\authors{Ferreira, Ricardo W.} \\
\authors{Medina, Alexandre F.} \\
\authors{Oliveira, Marco B. X.} \\
\authors{Vieira, Daniel} \\
\institution{Avibras Indústria Aeroespacial S.A.} \\
\institution{ricardo.ferreira@avibras.com.br; alexandre.correa@avibras.com.br; marco.oliveira@avibras.com.br; daniel.vieira@avibras.com.br} \\
\\
\abstract{\textbf{Abstract.} The wrap-around fins (WAF) configuration has been widely used in unguided rockets, antitank missiles and cruise missiles, with application from the early 1960's. Although it present equal longitudinal dynamics as its planar counterpart, the lateral dynamics presents new characteristics, such as the rolling moment observed even at zero angle of attack, and the roll reversal as the fin set accelerates from subsonic to supersonic flow, and vice-versa. This paper aims on analyzing the capability of recent computational fluid dynamics techniques on reproducing experimental results and, further on, study different means of reducing the induced roll moment observed on WAF, which can be detrimental to the missile's stability.}\\
\\
\keywords{\textbf{Keywords:} Wraparound fins, Rolling moment, Missile Aerodynamics, CFD}\\
\end{tabular}

\section{INTRODUCTION}

Widely used in the latest decades, tube rocket launchers has its applications from ground-launched saturation field-artillery rockets to air-launched weapons. However its application has been massive nowadays, the concept dates from earlier times, as its first documented application is from Imperial China \citep{Needham1974}, during the medieval Song dynasty. Later, in the western civilization, tube rocket launchers were used in the Napoleonic Wars, introduced by the British in 1804 with the Congreve rocket, devised by Sir William Congreve \citep{Congreve1814, Bailey2004}. The Congreve rocket was also applied during the American Civil War, used by the Confederate Army; while the Union used Hale patent rocket launcher with fin stabilized rockets. The use of rocket artillery system was later enhanced during World War II with the use of solid fuel rockets for anti-aircraft batteries: in the United Kingdom it was first applied to fixed unrotated systems and then with the static Z Battery. The system was then developed to be mounted on self-propelled systems, such as the truck mounted launcher Katyusha, from the Soviets, and the tank mounted Calliope, from the United States Army. Recent application of tube launchers range from small to large scale systems, from the short range shoulder-fired RPG and Bazookas, both anti-tank weapons; rocket pod tube launchers design to attack aircrafts and helicopters, for close air support; and large-scale multiple rocket launchers systems (MLRS) for long range applications and field saturation. Although the use of tube-launchers was first applied to unguided projectiles, nowadays it has been largely used to guided weapons systems. As \cite{Seginer1983a} explain, tube-launchers improves the system packaging and increases the rocket motor reliability. However, as is also seems in container-launchers, the aerodynamic fins need to be folded to fit inside the tube cylinder or container. For tube-launchers, the best configuration devised is the wrap-around fins (WAF). In this configuration the fins can be folded over the cylindrical body of the projectile, which are then deployed after launch.

The WAF configuration has been widely used in unguided rockets, antitank missiles and cruise missiles, with application from the early 1960's. It has the same longitudinal characteristics as flat fins, as presented in the studies from  \cite{Featherstone1960,Dahlke1973} and \cite{Dahlke1976,Dahlke1990}. Although the longitudinal characteristics are equal between both configuration, \cite{Featherstone1960} was one the first to show that this was not true for the lateral characteristics, which presented a rather unpredictable behavior for WAF. One of these behavior includes the rolling moment at zero angle of attack, even with zero fin cant angle. This behavior was observed in the experiments from \cite{Dahlke1976}, which presents a complete study ranging from subsonic to supersonic Mach numbers for the TTCP (Technical Cooperation Program) standard rocket. This study allowed not only to confirm this rolling behavior of WAF, but also to demonstrate the roll reversal during acceleration or deceleration. The TTCP was choosen as standard geometry for the later studies, such as the work from \cite{Seginer1983a,Seginer1983}, which attempted to model the roll characteristic analytically and numerically, as this behavior could affect the missile stability, leading to roll resonance or roll-yaw coupling, which would affect the projectile precision. They found that the radial velocity component induced on the fins by the converging separated wake at the projectile's base could explain the rolling moment observed for WAF configurations for subsonic models, but could not explain the reversal roll at supersonic velocities. \cite{Seginer1983a,Seginer1983} also show, by means of their analytical model, that moving the fins upstream, by two diameters, would reduce this moment. The rolling moment would also be reduced by forward sweeping the fin trailing edge, as it would reduce the interaction with the base flow field. \cite{Seginer1983a} cites that the concept moving the WAF upstream was already applied in the Vought's MLRS missile in 1980, although no explanation was given at the time.

The evolution of computational capabilities after the 1980's allowed the use of computational fluid dynamics (CFD) to model the Euler and Navier-Stokes equations for different aerodynamic studies, including the analysis of the flow field around WAF. One of the first studies to investigate WAF using the Navier-Stokes equations on CFD was \cite{Edge1994}, which used a three-dimensional laminar Navier-Stokes code. \cite{Edge1994} used a similar setup as \cite{Dahlke1976} to study the flow behavior for the TTCP from subsonic to supersonic Mach numbers, with the objective to better predict the crossover point for the reversal roll. \cite{Edge1994} found a trend similar to the experiments, although discrepancies are found in the magnitude of the rolling moment at Mach numbers lower than 1.5 and greater than 2.0. Later, \cite{Paek1999} showed that the geometry modeling could lead to the overestimation of the rolling coefficient, as \cite{Edge1994} used a blunt leading and trailing edges, which did not represented the fin geometry of the TTCP. In their work, \cite{Paek1999} used a three-dimensional Euler code, which produced results in agreement with the experiments from \cite{Dahlke1976}, in both trend and magnitude, which shows that the use of the Euler equations with a proper mesh can produce results with a comparable accuracy. Similar formulation and results are found in \cite{Kim2012}. More recently, \cite{Li2015} performed CFD simulations for transonic and supersonic velocities using the full Navier-Stokes equations, applying the $\kappa-\omega$ SST turbulence model \citep{menter93,menter04}. As they show, a similar trend to the experiment is found, with better agreement found for Mach number above 2.5. 

The ability of both Euler and Navier-Stokes equations, modeled in present CFD methodologies, shows that it became an important tool on predicting the aerodynamic characteristics of WAF, as the behavior found is similar to that in experimental studies. Hence, our first objective is to reproduce the results found in \cite{Dahlke1976} using the Reynolds-averaged Navier-Stokes equation (RANS) and to achieve a small error margin, as observed by \cite{Paek1999}. In sequence, the second objective is to analyze, as proposed by \cite{Seginer1983a}, the effect of moving the fin set upstream the rocket. \cite{Seginer1983a} mathematical model already shows that the roll moment is expected to reduce, and the concept is already proved by Vought's MLRS missile. Hence, our objetive by this study is to further understand the flow field and how it can induce the rolling moment on WAF.

\section{METHODOLOGY}

This study, as the ones previous to it, will analyze the flow field around the standard wrap-around fin projectile proposed by the Technical Cooperation Program (TTCP). The geometry of the standard TTCP configuration is presented in Figure \ref{fig:TTCP}, the dimensions are normalized by the caliber of the projectile (1 caliber = 101.6 mm). As explained by \cite{Paek1999}, the fin edge geometry can affect the flow as a blunt edge could lead to flow separation, hence it is paramount that the fin geometry reproduces the exact characteristics such as the TTCP WAF, presented in Figure \ref{fig:WAF}.

The commercial software Metacomp CFD++ will be used to solve the Reynolds-averaged Navier-Stokes equation to achieve a steady state solution. As \cite{Li2015}, the $\kappa-\omega$ SST will be used for the turbulence modeling. A unstructured mesh will be used to discretize the computational domain, which has a spherical geometry with radius of 50 calibers, which is sufficient to avoid any influence of the outer boundaries on the vicinities of the standard model.

\begin{figure}[h!]
\centering
\includegraphics[angle=0, scale=0.75]{TTCP}
\caption{TTCP standard projectile geometry.}
\label{fig:TTCP}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[angle=0, scale=0.75]{WAF}
	\caption{TTCP standard WAF geometry.}
	\label{fig:WAF}
\end{figure}

\newpage

\section{COMPARATIVE STUDY}

The comparative study is imperative to show the ability of our setup to reproduce both experimental and numerical results of previous studies, being the work from \cite{Dahlke1976}, \cite{Paek1999} and \cite{Li2015} our references to evaluate our setup accuracy. The validation will cover not only previous studies, but also, as required for numerical analyses, a mesh dependency study, to ensure our results can be achieved in an efficient fashion. Following the comparative study, the roll moment analysis will be made by moving the WAF set upstream, by 1, 2 and 3 calibers to investigate how the roll of the projectile can be affected by the base wake.

\begin{figure}[h!]
	\centering
	\includegraphics[angle=0, scale=0.8]{Reference}
	\caption{Roll moment coefficient vs Mach Number for experimental and numerical references.}
	\label{fig:REF}
\end{figure}

%\begin{equation}
%[M]\{\ddot{x}\}+[C]\{\dot{x}(t)\}+[K]\{x(t)\}={f(t)} 
%\label{eq1}
%\end{equation}

%\begin{equation}
%\mathbf{M\ddot{x}}(t)+\mathbf{C\dot{x}}(t)+\mathbf{Kx}(t)=\mathbf{f}(t) 
%\label{eq2}
%\end{equation}


%\begin{figure}[h!]
%\centering
%\includegraphics[angle=0, scale=0.320]{figure.jpeg}
%\caption{United States crude oil imports from Norway versus number of drivers killed in collision with railway train. Available from: http://tylervigen.com/spurious-correlations}
%\label{fig1}
%\end{figure}

\section{ACKNOWLEDGEMENTS}
The authors would like to thank Avibras Indústria Aeroespacial S.A. for the means to perform this study.

\newpage

\section{REFERENCES} 
\bibliographystyle{abcm}
\renewcommand{\refname}{}
\bibliography{bibfile}

\end{document}
